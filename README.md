# Togago

``` bash
For Windows:
copy .env.example .env
For Linux:
cp .env.example .env
```

``` bash
update .env file
composer update
npm update
php artisan generate:key 
php artisan migrate:fresh --seed
```

``` bash
php artisan serve
npm run watch
```