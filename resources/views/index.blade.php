<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <meta name="description" content="">
	    <meta name="author" content="">
        <title>Togago</title>
        <link rel="stylesheet" href="{{asset('/css/app.css')}}">
        <link rel="icon" href="{!! asset('images/togago_logo.png') !!}" />

    </head>
    <body>
        <div id="root">
            <app-navigation></app-navigation>
                <transition name="router-anims" enter-active-class="animated fadeIn" mode="out-in">
                <div class="togago-wrapper">
                    <router-view></router-view>
                </div>
                </transition>
            <app-footer></app-footer>
        </div>
        <script>
            var homeurl = "{{url('/')}}"
            var supportedLangs = {!! collect($supportedLangs)->toJson() !!}
            var currentLang = "{{ app()->getLocale() }}";
        </script>
        <script src="{{url('/js/bundle.min.js')}}"></script>
    </body>
</html>
