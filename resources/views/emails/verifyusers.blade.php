<div>
  Hi {{$name}},
  <br>
  Thank you for registering with us please verify your account by clicking link below.<br><br>
  <a href="{{url('/auth/verify?code=' . $verification_code . '&email=' . $email)}}">Verify</a>
  </div>