import Vue from "vue";
import Vuex from "vuex";
import createPersistedState from "vuex-persistedstate";
import { cpus } from "os";
Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        auth: {},
        flight: {},
        hotel: {},
    },
    mutations: {
        setUserdata(state, payload) {
            state.auth = payload;
        },
        //Flight
        setSelectedFlightId(state, payload) {
            state.flight.selecteditemId = payload;
        },
        setSearchFlightToken(state, payload) {
            state.flight.token = payload;
        },
        resetSearchFlightToken(state) {
            state.flight.token = null;
        },
        setFlightSearchdata(state, payload) {
            state.flight.searchdata = payload;
        },
        setFlightPrebook(state, payload) {
            state.flight.prebook = payload;
        },
        setSelectedFlight(state, selectedFlight) {
            state.flight.selectedFlight = selectedFlight;
        },
        setFlightBookdata(state, bookdata) {
            state.flight.bookdata = bookdata
        },
        setFlightPassengers(state, passengers) {
            state.flight.passengers = passengers
        },
        //Hotel
        setHotelSearchdata(state, payload) {
            state.hotel.searchdata = payload;
        },
        setSearchHotelToken(state, payload) {
            state.hotel.token = payload;
        },
        //Other
        reset(state) {
            Object.assign(state, {
                auth: {},
                flight: {},
                hotel: {},
            })
        },
        resetAuth(state) {
            Object.assign(state, {
                auth: {}
            })
        }
    },
    actions: {
        setUserdata({commit}, auth) {
            commit("setUserdata", auth);
        },
        //Flight
        setSelectedFlightId({commit}, itemId) {
            commit("setSelectedFlightId", itemId);
        },
        setSearchFlightToken({commit}, searchToken) {
            commit("setSearchFlightToken", searchToken);
        },
        setFlightSearchdata({commit}, searchData) {
            commit("setFlightSearchdata", searchData)
        },
        setFlightPrebook({commit}, prebook){
            commit("setFlightPrebook", prebook)
        },
        setSelectedFlight({commit}, selectedFlight){
            commit("setSelectedFlight", selectedFlight)
        },
        setFlightBookdata({commit}, bookdata){
            commit("setFlightBookdata", bookdata)
        },
        setFlightPassengers({commit}, passengers){
            commit("setFlightPassengers", passengers)
        },
        resetSearchFlightToken({commit}) {
            commit("resetSearchFlightToken")
        },
        // Hotel
        setHotelSearchdata({commit}, searchData) {
            commit("setHotelSearchdata", searchData)
        },
        setSearchHotelToken({commit}, searchToken) {
            commit("setSearchHotelToken", searchToken);
        },
        //Other
        reset({commit}){
            commit("reset")
        },
        resetAuth({commit}){
            commit("resetAuth")
        }
    },
    getters: {
        getUserdata(state) {
            return state.auth;
        },
        //Flight
        getSelectedFlightId(state) {
            return state.flight.selecteditemId
        },
        getSearchFlightToken(state) {
            return state.flight.token
        },
        getFlightSearchdata(state) {
            return state.flight.searchdata
        },
        getFlightPrebook(state) {
            return state.flight.prebook
        },
        getSelectedFlight(state) {
            return state.flight.selectedFlight
        },
        getFlightBookdata(state) {
            return state.flight.bookdata
        },
        getFlightPassengers(state) { 
            return state.flight.passengers
        },
        // Hotel
        getHotelSearchdata(state) {
            return state.hotel.searchdata
        },
        getSearchHotelToken(state) {
            return state.hotel.token
        }
    },
    plugins: [createPersistedState({
        storage: window.sessionStorage
    })]
});

export default store;
