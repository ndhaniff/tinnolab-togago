$(function () {
    "use strict";
    $(function () {
        $(".preloader").fadeOut();
    });

    // ==============================================================
    // Disable Back Button
    // ==============================================================

    history.pushState(null, null, location.href);
    window.onpopstate = function () {
        history.go(1);
    };
 
    window.onunload = function () {
        null
    }
    
    // ==============================================================
    // Repair Dropdown Bug
    // ==============================================================
    $('a.dropdown-toggle').click(function () {
        $(this).next('.dropdown-menu').css({display: "none"});
    });


});
