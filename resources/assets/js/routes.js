import VueRouter from "vue-router";
import helper from "./services/helper";

let routes = [{
        path: "/:lang",
        component: require("./layouts/Master"),
        meta: {
            requiresGuest: true,
            title: "Togago"
        },
    },
    {
        path: "/:lang/auth/verify",
        component: require('./auth/Verify'),
    },
    {
        path: "/:lang/auth/register",
        component: require('./auth/Register'),
    },
    {
        path: "/:lang/auth/forgot",
        component: require('./auth/Forgot'),
    },
    {
        path: "/:lang/flight/search",
        component: require('./flight/Results.vue'),
        meta: {
            title: "Togago | Search",
            requireSession: true
        }
    },
    {
        path: "/:lang/flight/prebooking",
        component: require('./flight/Prebook.vue'),
        meta: {
            title: "Togago | Prebooking",
            requireSession: true
        }
    },
    {
        path: "/:lang/flight/booking",
        component: require('./flight/Booking.vue'),
        meta: {
            title: "Togago | Booking",
            requireSession: true
        }
    },
    {
        path: "/:lang/flight/payment",
        component: require('./flight/Payment.vue'),
        meta: {
            title: "Togago | Payment",
            requireSession: true
        }
    },
    {
        path: "/:lang/flight/success/:ref_id",
        component: require('./flight/Success.vue'),
        meta: {
            title: "Togago | Payment Successfull",
            requireRefID: true
        }
    },
    {
        path: "/:lang/flight/unsuccessfull/:ref_id",
        component: require('./flight/Success.vue'),
        meta: {
            title: "Togago | Payment Unsuccessfull",
            requireRefID: true
        }
    },
    // Hotel
    {
        path: "/:lang/hotel/results",
        component: require('./hotel/Results.vue'),
        meta: {
            title: "Togago | Results"
        }
    },
    //Account
    {
        path: "/:lang/account/my-bookings",
        component: require('./account/MyBookings.vue'),
        meta: {
            title: "Togago | My Bookings",
            requireAuth: true
        }
    },
    {
        path: "/:lang/account/my-account",
        component: require('./account/MyAccount.vue'),
        meta: {
            title: "Togago | My Account",
            requireAuth: true
        }
    },
    // Other
    {
        path: "*",
        component: require("./errors/page-not-found"),
    }

]

const router = new VueRouter({
    scrollBehavior() {
        return { x: 0, y: 0 };
    },
    routes,
    linkActiveClass: "active",
    mode: "history",
});

router.beforeEach((to, from, next) => {
    //Change Title
    document.title = to.meta.title

    /**
     * FLIGHT ROUTE MIDDLEWARE
     */
    //search session middleware
    if (to.matched.some(m => m.meta.requireSession)) {
        let session = JSON.parse(sessionStorage.getItem('vuex'))
        
        if(!session) {
            toastr["error"]("Please make a search")
            return next({
                path: "/" + to.params.lang
            });
        }
        else if(session && !("searchdata" in session.flight)) {
            toastr["error"]("Please make a search")
            return next({
                path: "/" + to.params.lang
            });
        }
    }

    //authorize route
    if (to.matched.some(m => m.meta.requireAuth)) {
        return helper.check().then(response => {
            if (!response) {
                return next({
                    path: "/" + to.params.lang
                });
            }

            return next();
        });
    }

    //payment success route
    if (to.matched.some(m => m.meta.requireRefID)) {
        return helper.checkPaymentStatus(to.params.ref_id).then(response => {
            if (!response.success && response.status != "00") {
                toastr["error"]("Unauthorize Request")
                return next({
                    path: "/" + to.params.lang
                });
            }

            return next();
        });
    }

    return next();
});

export default router;
