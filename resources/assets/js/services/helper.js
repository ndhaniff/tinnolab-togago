export default {
    check() {
        return axios.post('/api/auth/check').then(response => {
            return !!response.data.authenticated;
        }).catch(error => {
            return false;
        });
    },
    checkPaymentStatus(ref_id) {
        return axios.post('/api/booking/checkPaymentStatus',{ref_id}).then(response => {
            return response.data
        }).catch(error => {
            return false;
        });
    },
    keepOpen(className) {
        jQuery(".dropdown-toggle").on("click", function (e) {
            $(this)
                .next()
                .toggle();
        });
        jQuery(".dropdown-menu." + className).on("click", function (e) {
            e.stopPropagation();
        });
    },
    setOriginDestination(searchData) {
        return {
            origin: lang.get('iata.'+searchData.origin).split(",")[0] + ` ( ${searchData.origin} )`,
            destination: lang.get('iata.'+searchData.destination).split(",")[0] + ` ( ${searchData.destination} )`
        }
    },
    getJourneytime(min) {
        let hours = Math.floor(min / 60);
        let minutes = min % 60;
  
        if (hours == 1) {
          hours += " hour";
        }
        else if (hours == 0) {
            hours = "";
        }
        else if (hours > 1) {
          hours += " hours";
        }
        if (minutes == 0) {
          minutes = "";
        } else {
          minutes += " minutes";
        }
  
        return {
          hours,
          minutes
        };
      },

    getTimeFromMins(mins) {
        var h = mins / 60 | 0,
            m = mins % 60 | 0;
        return moment.utc().hours(h).minutes(m).format("hh:mm");
    }
}
