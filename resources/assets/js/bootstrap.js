import Vue from "vue";
import VueRouter from "vue-router";
import axios from "axios";
import Vuelidate from "vuelidate";
import AppNavigation from "./components/Navigation"
import AppFooter from "./layouts/Footer"
import _ from "lodash";
import IdleVue from 'idle-vue'
import VuePaginate from 'vue-paginate'
import VueCookies from "vue-cookies";

const eventsHub = new Vue()

Vue.use(VueRouter);
Vue.use(Vuelidate);
Vue.use(VuePaginate);
Vue.use(VueCookies);
// Vue.use(IdleVue, {
//     eventEmitter: eventsHub,
//     idleTime: 60000
//   })
Vue.component('app-navigation',AppNavigation);
Vue.component('app-footer',AppFooter);

window.$cookies.config('12h');
window.Vue = Vue;
window.lang = Lang
window._ = _;
window.axios = axios;

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
window.axios.defaults.headers.common["Authorization"] =
    "Bearer " + localStorage.getItem("auth_token");

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from 'laravel-echo'

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: process.env.MIX_PUSHER_APP_KEY,
//     cluster: process.env.MIX_PUSHER_APP_CLUSTER,
//     encrypted: true
// });
