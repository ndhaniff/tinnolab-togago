<?php

return [
  'guest' => 'Guest',
  'room' => '[0,1] Room|Rooms',
  'passenger' => [
    'adult' => '[0,1] Adult|Adults',
    'child' => '[0,1] Child|Children',
    'infant' => '[0,1] Infant|Infants',
  ],
  'class' => [
    'any' => 'Any',
    'economy' => 'Economy',
    'premium' => 'Premium Economy',
    'business' => 'Business',
    'first' => 'First Class',
  ],
  'roomType' => [
    'single' => 'Single bed',
    'double' => 'Double bed',
    'twin' => 'Twin bed',
    'triple' => 'Triple bed',
    'quad' => 'Quad bed',
    'solo' => 'Twin bed for solo use',
  ],
  'placetogo' => 'City, place, hotel to stay',
  'selectage' => 'Age',
];
