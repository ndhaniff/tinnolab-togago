<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'booking' => 'My Bookings',
    'language' => 'English',
    'login' =>  [
      'login'=> 'Sign in',
      'email' => 'Email',
      'password' => 'Password',
      'rememberme' => 'Remember me',
      'forgotpw' => 'Forgot Password?',
      'signin' => 'Sign in',
      'signinyippi' => 'Sign in with Yippi',
      'notmember' => 'Not a member?',
      'register' => 'Register',
      'alreadyregister' => 'Already Registered?',
      'noverifycode' => 'Verification code not received?',
      'resend' => 'Resend',
    ],
    'error' => [
      'emailrequired' => 'Email is required',
      'emailvalid' => 'Please enter valid email',
      'pwrequired' => 'Password is required',
      'pwminlength' => 'Password must be more than 6 characters',
      'allrequired' => 'Please correct all required fields',
    ]
];
