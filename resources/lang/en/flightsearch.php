<?php
return [
  'flights' => 'Flights',
  'departure' => 'Departure',
  'arrival' => 'Arrival',
  'searchflight' => 'Search Flight',
  'return' => 'Return',
  'passenger' => [
    'adult' => '[0,1] Adult|Adults',
    'adultage' => '(12 years & above)',
    'child' => '[0,1] Child|Children',
    'childage' => '(0-12 years)',
    'infant' => '[0,1] Infant|Infants',
    'infantage' => 'Child (2-12 years), Infant (Below 2 years)',
  ],
  'selectage' => 'Select Age',
  'class' => [
    '0' => 'Any',
    'E' => 'Economy',
    'P' => 'Premium Economy',
    'B' => 'Business',
    'F' => 'First Class',
  ],
  'results' => [
    'includes' => 'INCLUDES',
    'fare' => 'FARE',
    'taxes' => 'TAXES',
    'more' => 'More',
    'less' => 'Less'
  ]
];
