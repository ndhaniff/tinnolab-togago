<?php
return [
  'flights' => '机票',
  'departure' => '出发',
  'arrival' => '到达',
  'searchflight' => '搜索航班',
  'return' => '往返',
  'passenger' => [
    'adult' => '成人',
    'adultage' => '(12岁以上)',
    'child' => '儿童',
    'childage' => '(0-12岁)',
    'infant' => '婴儿',
    'infantage' => '儿童 (2-12岁), 婴儿 (2岁以下)',
  ],
  'selectage' => '请选择年龄',
  'class' => [
    '0' => '任何舱位',
    'E' => '经济舱',
    'P' => '高端经济舱',
    'B' => '商务舱',
    'F' => '头等舱',
  ],
  'results' => [
    'includes' => 'INCLUDES',
    'fare' => 'FARE',
    'taxes' => 'TAXES',
    'more' => 'More',
    'less' => 'Less'
  ],
];
