<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => '密码必须至少为六个字符并与确认匹配。',
    'reset' => '您的密码已重置！',
    'sent' => '我们已通过电子邮件发送您的密码重置链接！',
    'token' => '此密码重置令牌无效',
    'user' => "无法寻获该电子邮件地址用户",
    'forgotpw' => '忘记密码',
    'repeatpw' => '重复输入密码',
    'next' => '下一步',
    'back' => '返回',
    'submit' => '提交',
    'error' => [
      'correctemail' =>'请输入正确电邮',
      'pwlength' =>'密码必须超过8个字符',
      'repeatpw' =>'需要重复密码',
      'pwnotmatch' =>'密码不匹配',
    ]

];
