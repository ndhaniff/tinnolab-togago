<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'booking' => '我的预订',
    'language' => '简体中文',
    'login' =>  [
      'login' => '登录',
      'email' => '电子邮件',
      'password' => '密码',
      'rememberme' => '记住账号',
      'forgotpw' => '忘记密码?',
      'signin' => '登录',
      'signinyippi' => '使用Yippi登录',
      'notmember' => '非会员?',
      'register' => '注册',
      'alreadyregister' => '已是会员?',
      'noverifycode' => '未收到验证码?',
      'resend' => '重发',
    ],
    'error' => [
      'emailrequired' => '请输入电子邮件',
      'emailvalid' => '请输入有效的电子邮件',
      'pwrequired' => '请输入密码',
      'pwminlength' => '密码必须超过6个字符',
      'allrequired' => '请更正所有必填项目',
    ]
];
