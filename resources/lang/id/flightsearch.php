<?php
return [
  'flights' => 'Penerbangan',
  'departure' => 'Keberangkatan',
  'arrival' => 'Ketibaan',
  'searchflight' => 'Cari Penerbangan',
  'return' => 'Pulang',
  'passenger' => [
    'adult' => 'Dewasa',
    'adultage' => '(12 tahun ke atas)',
    'child' => 'Anak-anak',
    'childage' => '(0-12 tahun)',
    'infant' => 'Bayi',
    'infantage' => 'Anak (2-12 tahun), Bayi (Di bawah 2 tahun)',
  ],
  'selectage' => 'Pilih Umur',
  'class' => [
    '0' => 'Apa Saja',
    'E' => 'Ekonomi',
    'P' => 'Ekonomi Premium',
    'B' => 'Bisnis',
    'F' => 'Kelas Utama',
  ],
  'results' => [
    'includes' => 'INCLUDES',
    'fare' => 'FARE',
    'taxes' => 'TAXES',
    'more' => 'More',
    'less' => 'Less'
  ],
];
