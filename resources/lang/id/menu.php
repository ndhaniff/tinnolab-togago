<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'booking' => 'Tempahan Saya',
    'language' => 'Bahasa',
    'login' =>  [
      'login' => 'Masuk',
      'email' => 'Email',
      'password' => 'Kata sandi',
      'rememberme' => 'Ingat saya',
      'forgotpw' => 'Lupa kata sandi?',
      'signin' => 'Masuk',
      'signinyippi' => 'Masuk dengan Yipi',
      'notmember' => 'Belum menjadi anggota?',
      'register' => 'Daftar',
      'alreadyregister' => 'Sudah daftar?',
      'noverifycode' => 'Kode verifikasi tidak diterima?',
      'resend' => 'Kirim ulang',
    ],
    'error' => [
      'emailrequired' => 'Email diperlukan',
      'emailvalid' => 'Silakan masukkan email yang valid',
      'pwrequired' => 'Kata sandi diperlukan',
      'pwminlength' => 'Kata sandi harus lebih dari 6 karakter',
      'allrequired' => 'Harap perbaiki semua bidang yang wajib diisi',
    ]
];
