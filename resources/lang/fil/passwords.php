<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Ang mga password ay dapat na hindi bababa sa anim na character at tumutugma sa kumpirmasyon.',
    'reset' => 'Na-reset ang iyong password!',
    'sent' => 'Nag-e-mail kami ng iyong pag-reset ng password na link!',
    'token' => 'Di-wasto ang pag-reset ng token ng password.',
    'user' => "Hindi namin mahanap ang isang user na may e-mail address na iyon.",
    'forgotpw' => 'Nakalimutan ang password',
    'repeatpw' => 'Ulitin ang password',
    'next' => 'Susunod',
    'back' => 'Bumalik',
    'submit' => 'Ipasa',
    'error' => [
      'correctemail' =>'Mangyaring ipasok ang tamang email',
      'pwlength' =>'Ang password ay dapat na higit sa 8 character',
      'repeatpw' =>'Kinakailangan ang Ulitin ang Password',
      'pwnotmatch' =>'Hindi tumutugma ang password',
    ]

];
