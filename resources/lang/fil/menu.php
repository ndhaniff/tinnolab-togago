<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'booking' => 'Ang Booking Ko',
    'language' => 'Filipino',
    'login' =>  [
      'login'=> 'Pumasok',
      'email' => 'E-mail',
      'password' => 'Password',
      'rememberme' => 'Tandaan mo ako',
      'forgotpw' => 'Nakalimutan ang password??',
      'signin' => 'Mag-sign in',
      'signinyippi' => 'Mag-sign in gamit ang Yippi',
      'notmember' => 'Hindi miyembro?',
      'register' => 'Magparehistro',
      'alreadyregister' => 'Nakarehistro na?',
      'noverifycode' => 'Hindi natanggap ang code ng pagpapatunay?',
      'resend' => 'Ipadala muli',
    ],
    'error' => [
      'emailrequired' => 'Kinakailangan ang email',
      'emailvalid' => 'Mangyaring ipasok ang wastong email',
      'pwrequired' => 'Kinakailangan ang password',
      'pwminlength' => 'Ang password ay dapat na higit sa 6 na mga character',
      'allrequired' => 'Pakitama ang lahat ng kinakailangang field',
    ]
];
