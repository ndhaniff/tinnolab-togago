<?php
return [
  'flights' => 'Flight',
  'departure' => 'Pag-alis',
  'arrival' => 'Pagdating',
  'searchflight' => 'Maghanap Ng Paglipad',
  'return' => 'Pag-balik',
  'passenger' => [
    'adult' => 'Matanda',
    'adultage' => '(12 taong gulang at sa itaas)',
    'child' => 'Mga bata',
    'childage' => '(0-12 taong gulang)',
    'infant' => 'Mga Sanggol',
    'infantage' => 'Bata (2-12 taong gulang), Sanggol (Sa ilalim ng 2 taong gulang)',
  ],
  'selectage' => 'Piliin ang Edad',
  'class' => [
    '0' => 'Anuman',
    'E' => 'Ekonomiya',
    'P' => 'Premium Economy',
    'B' => 'Negosyo',
    'F' => 'Primera klase',
  ],
  'results' => [
    'includes' => 'INCLUDES',
    'fare' => 'FARE',
    'taxes' => 'TAXES',
    'more' => 'More',
    'less' => 'Less'
  ]
];
