<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::group([
    'prefix' => 'auth',
], function () {
    Route::post('register', 'AuthController@register')->name('register');
    Route::post('login', 'AuthController@login')->name('login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::get('me', 'AuthController@me');
    Route::get('verify', 'AuthController@verify')->name('verify');
    Route::post('check', 'AuthController@check')->name('check');
    Route::post('forgot', 'AuthController@forgotPassword')->name('forgot');
});

Route::group([
    'prefix' => 'flight',
], function () {
    Route::post('search', 'FlightController@search')->name('flight.search');
    Route::post('filter', 'FlightController@filter')->name('flight.filter');
    Route::post('getselected', 'FlightController@getSelected')->name('flight.select');
    Route::post('booking', 'FlightController@saveBookingInfo')->name('flight.booking');
    Route::post('finalize', 'FlightController@finalizeBooking')->name('flight.finalize');
    Route::post('process_payment', 'FlightController@processPayment')->name('flight.process_payment');
});
Route::group([
    'prefix' => 'booking',
], function () {
    Route::post('checkPaymentStatus', 'BookingController@checkPaymentStatus')->name('Booking.checkPaymentStatus');
});
Route::group([
    'prefix' => 'hotel',
], function () {
    Route::get('getAccessToken', 'HotelController@getAccessToken')->name('hotel.getAccessToken');
    Route::post('searching', 'HotelController@search')->name('hotel.searching');
    Route::get('result', 'HotelController@result')->name('hotel.result');
    Route::get('test', 'HotelController@test')->name('hotel.test');
});
Route::group([
    'prefix' => 'account',
], function () {
    Route::get('flightbooking', 'BookingController@flight')->name('flightbooking.list');
    Route::get('bookinginfo', 'BookingController@getUserBookingInfo')->middleware('jwt.auth')->name('booking.info');
    Route::post('addchange_email', 'AuthController@addChangeEmail')->middleware('jwt.auth')->name('addchange.email');
});

Route::group([
    'prefix' => 'yippi',
], function () {
    Route::post('weblogin', 'YippiController@weblogin')->name('yippi.weblogin');
});