<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class sendCustomerTempPassword extends Mailable
{
    use Queueable, SerializesModels;

    public $subject;
    public $name;
    public $email;
    public $password;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($args)
    {
        $this->subject = $args['subject'];
        $this->name = $args['name'];
        $this->email = $args['email'];
        $this->verification_code = $args['password'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(['address' => 'noreply@togago.com', 'name' => 'Togago'])
        ->subject($this->subject)
        ->view('emails.sendCustomerTempPassword');
    }
}