<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(190);
        \View::composer('index', function ($view) {
            $data = collect(\LaravelLocalization::getSupportedLocales())->map(function ($lang, $key) {
                return [
                    'code' => $key,
                    'native' => $lang['native']
                ];
            })->values();
            $view->with('supportedLangs', $data);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
