<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
      'booking_id', 'ref_id', 'type', 'status', 'details'
    ];

    public function getStatusCodeAttribute()
    { 
      $details = unserialize($this->details);
      return $details['STATUS_CODE'];
    }
}
