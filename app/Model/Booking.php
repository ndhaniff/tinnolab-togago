<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{   
    protected $table = "bookings";
    protected $fillable = ['has_paid', 'has_book'];

    public function payment()
    {
        return $this->hasOne('App\Model\Payment', 'booking_id', 'booking_id');
    }
}
