<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Flight extends Model
{
    protected $table = "flights";

    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    public function booking()
    {
        return $this->hasOne('App\Model\Booking', 'booking_id', 'booking_id');
    }

    public function getBookDataAttribute() 
    {   
        $details = json_decode($this->details, true);

        if($this->isRoundTrip) {
            $midIndex = count($details['segments']) / 2 - 1;
        }
        else {
            $midIndex = count($details['segments']) - 1;
        }

        return [
            'date_created' => $this->created_at,
            'origin' => $this->origin,
            'destination' => $this->destination,
            'departure' => $this->departure,
            'arrival' => $details['segments'][$midIndex]['destination']['date'],
            'departureDateReturn' => $this->departureDateReturn,
        ];
    }
}
