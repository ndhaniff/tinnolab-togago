<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = [
        "user_id", 
        "first_name",
        "last_name",
        "country_code",
        "mobile_prefix",
        "mobile_number",
        "zip",
        "city",
        "address",
        "booking_info"
    ];
}
