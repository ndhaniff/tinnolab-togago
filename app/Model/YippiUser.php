<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class YippiUser extends Model
{
    protected $table = "yippi_users";
    protected $fillable = [
        "uid","email","username","phone","access_token"
    ];
    
}
