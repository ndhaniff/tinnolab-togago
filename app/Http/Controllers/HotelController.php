<?php

namespace App\Http\Controllers;

use App\Helper\Hotel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HotelController extends Controller
{
    public function getAccessToken(Hotel $hotel)
    {
        return $hotel->getAccessToken();
    }
    public function search(Request $request, Hotel $hotel)
    {
        $this->hotelData = [];
        $this->filterData = [];
        $token = $request->token;

        $rooms = [
            [
                'roomType' => "single",
                'children' => [],
            ],
        ];
        $params = [
            'token' => $token,
            'cityId' => "104719",
            'checkIn' => "2019-03-10",
            'checkOut' => "2019-03-12",
            'rooms' => $rooms,
            'category' => "0",
            'nationality' => "",
            'residence' => "",

        ];

        /* CACHING SEARCH FOR 3 MINUTES */
        $key = http_build_query([
            'searchToken' => $token,
        ], 'flags_');

        /* if (!Cache::has($key)) {
        $searchData = $hotel->search($params);
        Cache::put($key, $searchData, 3);
        $searchData = json_decode($searchData, true);
        } else {
        $searchData = json_decode(Cache::get($key), true);
        } */

        $searchData = $hotel->search($params);
        $searchData = json_decode($searchData, true);

        \Debugbar::info($params);
        \Debugbar::info($searchData);

        if (array_key_exists('error', $searchData)) {
            return [
                'success' => false,
                'code' => $searchData["error"]["code"],
                'message' => $searchData["error"]["message"],
            ];
        }

        $entities = array_get($searchData, 'data.entities');
        $this->filterData = array_get($searchData, 'data.filters');
        \Debugbar::info($searchData);
        $this->iterateEntitiesData($entities);

        return [
            'success' => true,
            'hoteldata' => $this->hotelData,
            'filterdata' => $this->filterData,
            "token" => $token,
        ];
    }

    public function result(Request $request, Hotel $hotel)
    {
        $this->hotelData = [];

        $token = $request->token;
        $currentPage = $request->page;
        $finished = false;

        $params = [
            'token' => $token,
            'page' => $currentPage,
            'mapmarkers' => $request->mapmarkers,
            'filter[category]' => $request->category,
            'filter[price]' => $request->price,
            'filter[hotelname]' => $request->hotelname,
        ];

        /* CACHING SEARCH FOR 3 MINUTES */
        $key = http_build_query([
            'resultToken' => $token,
        ], 'flags_');

        /* if (!Cache::has($key)) { */
        $searchData = $hotel->result($params);
        //Cache::put($key, $searchData, 3);
        $searchData = json_decode($searchData, true);
        /* } else {
        $searchData = json_decode(Cache::get($key), true);
        } */
        if (array_key_exists('error', $searchData)) {
            return [
                "finised" => $finished,
                'code' => $searchData["error"]["code"],
                'message' => $searchData["error"]["message"],
            ];
        }

        $status = array_get($searchData, 'data.status');
        $properties = array_get($searchData, 'data.properties');
        $filters = array_get($searchData, 'data.filters');

        $entities = array_get($searchData, 'data.entities');

        if ($status['finished'] == false && $currentPage >= $properties['pages']) {
            if ($currentPage != 1) {
                return [
                    "token" => $token,
                    'status' => $status,
                    'properties' => $properties,
                    'filter' => $filters,
                    'hoteldata' => [],
                    "finised" => $finished,
                ];
            }else{
                
            }

        }

        $this->iterateEntitiesData($entities);

        if ($status['finished'] == true && $properties['page'] >= $properties['pages']) {
            $finished = true;
        }

        return [
            "token" => $token,
            'status' => $status,
            'properties' => $properties,
            'filter' => $filters,
            'hoteldata' => $this->hotelData,
            "finised" => $finished,
        ];

    }
    public function iterateEntitiesData($datas)
    {
        foreach ($datas as $data) {
            $dateGet = $data["rooms"][0]['cxlPolicy']['deadline'];
            $deadline = Carbon::parse($dateGet);
            if ($deadline->lessThan(Carbon::now())) {
                $deadline = "";
            } else {
                $deadline = $deadline->format('d-m-Y');
            }

            $args = [
                "itemId" => $data["itemId"],
                "category" => $data["properties"]['category']['numeric'],
                "name" => $data["properties"]['name'],
                "cityName" => $data["properties"]['location']['cityName'],
                "locName" => $data["properties"]['location']['name'],
                "images" => $data["properties"]['images'],
                "solutionName" => $data["rooms"][0]['name'],
                "deadline" => $deadline,
                "priceInfo" => $data["rooms"][0]['priceInfo'],
                "currency" => $data["rooms"][0]['price']['currency'],
                "price" => $data["rooms"][0]['price']['total'],
                "dtilLink" => $data['links'][0]['href'],
            ];
            //All rooms in the Hotel
            /* if (is_array($data["rooms"])) {
            $index = 1;
            foreach ($data["rooms"] as $key => $value) {
            $args['rooms'][] = $data["rooms"][$key];
            }
            } */
            array_push($this->hotelData, $args);
        }
    }

    public function test(Request $request)
    {
        $keyword = $request->input('q');
        $cities = DB::table('cities')->where('label', 'like', "%" . $keyword . "%")->get();

        // return response()->json([
        //     'label' => $cities->label,
        //     'label_zh' => $cities->label_zh,
        //     'id' => $cities->id,
        //     'category' => $cities->category,
        //     'categoryid' => $cities->categoryid,
        // ]);
        return response()->json(
            $cities
        );
    }
}
