<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\VerifyUsers;
use App\User;
use Illuminate\Support\Facades\DB;
use App\Events\EmailVerification;


class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => [
            'login',
            'register',
            'verify',
            'forgotPassword',
            'me'
        ]]);
    }

    /**
     * Register a user
     *
     * @param Request $request
     * @return void
     */
    public function register(Request $request)
    {   
        $credentials = $request->only('email', 'resend');

        if($request->resend){
            $rules = [
                'email' => 'required|email|max:255'
            ];
        }
        else{
            $rules = [
                'email' => 'required|email|max:255|unique:users'
            ];
        }
        

        $validator = Validator::make($credentials, $rules);

        if($validator->fails()) {
            return response()->json(['success'=> false, 'error'=> $validator->messages()]);
        }

        $email = $request->email;
        $name = explode('@', $email)[0];
        $password = str_random(8);
        $verification_code = str_random(30);
        $subject = "Please verify your email address.";

        $mailArgs = [
            "email" => $email,
            'name' => $name,
            "password" => $password,
            'verification_code' => $verification_code,
            'subject' => $subject
        ];

        if(!$request->resend){
            $user = User::create([
                'name' => $name, 
                'email' => $email, 
                'verified' => 0, 
                'verification_code' => $verification_code, 
                'password' => bcrypt($password)
            ]);

            $this->sendVerificationMail($mailArgs);

            return response()->json(['success'=> true, 'message'=> 'Thanks for signing up! Please check your email to complete your registration.']);
        }
        else{
            $user = $this->userExist($email);

            $this->sendVerificationMail($mailArgs);

            return response()->json(['success'=> true, 'message'=> 'We have send the new verification code to your email.']);
        }

        
    }

    /**
     * Mail the verification code
     *
     * @param array $args
     * @return void
     */
    public function sendVerificationMail(array $args) 
    {
        Mail::to($args['email'])->send(new VerifyUsers($args));
    }

    /**
     * Verify User
     */
    public function verify(Request $request)
    {
        $credentials = $request->only(['code','email']);

        $validator = Validator::make($credentials, ['code' => 'required', 'email' => 'required|email|max:255']);

        if($validator->fails()) {
            return response()->json(['success'=> false, 'error'=> $validator->messages()]);
        }

        $verification_code = $request->query('code');
        $email = $request->query('email');

        $user = $this->userExist($email);

        if(!$user->verification_code === $verification_code){
            return response()->json(['success'=> false, 'error'=> ['the verification code is not valid']]);
        }

        $user->update([
            'verified' => 1,
            'bind_status' => 1,
        ]);

        return response()->json(['success'=> true, 'message'=> 'Your account has been activated! Please login to view your account']);
    }

    /**
     * Forgot Password
     *
     * @param Request $request
     * @return Response Json
     */
    public function forgotPassword(Request $request)
    {   
        $credentials = $request->only('email','newpassword');

        $validator = Validator::make($credentials, ['newpassword' => 'required', 'email' => 'required|email|max:255']);

        if($validator->fails()) {
            return response()->json(['success'=> false, 'error'=> $validator->messages()]);
        }

        $user = $this->userExist($request->email);

        $user->update([
            'password' => bcrypt($request->newpassword)
        ]);

        return response()->json(['success'=> true, 'message'=> 'Your password has been reset, Please login with your new password']);
    }

    public function addChangeEmail(Request $request) 
    {
        $user = User::where('email',$request->email)->first();
        if($user) {
            return response()->json(['success' => false, 'error' => 'User with this email already exist!']);
        }
        else {
            $user = Auth::user();
        }

        if($request->type == "email") {
            //email the code
            event(new EmailVerification($user,$request->email));
            return response()->json(['success' => true, 'msg' => 'Successfully Send!']);
        }
        if($request->type == "verify") {
            //validate the code
            if($user->verification_code == $request->code) {
                //valid
                $user->update([
                    'email' => $request->email,
                    'verified' => 1,
                ]);
            }
            return response()->json(['success' => true, 'msg' => 'Successfully Verified!']);
        }
        if($request->type == "add-password") {
            //update the password
            $user->update([
                'password' => bcrypt($request->newpassword),
                'bind_status' => 3
            ]);

            return response()->json(['success' => true, 'msg' => 'Successfully Updated!']);
        }
    }

    /**
     * Check user exist by email
     *
     * @param [type] $email
     * @return User Collection | Response
     */
    public function userExist($email)
    {
        $user = User::where('email', '=', $email);

        if(!$user->exists()){
            return response()->json(['success'=> false, 'error'=> ['user does not exists']]);
        }

        return $user->first();
    }
    

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $credentials = request(['email', 'password']);
        
        if (!$token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        $user = $this->userExist(request('email'));

        if($user) {
            $roles = DB::table('roles as r')
                ->join('users_roles as ur', 'ur.role_id', '=', 'r.id')
                ->where('ur.user_id', '=', $user->id)->first();

            return response()->json([
                'success'=> true,
                'auth' => [
                    'access_token' => $token,
                    'name' => $user->name,
                    'email' => $user->email,
                    'role' => $roles->name,
                    'bind_status' => $user->bind_status,
                    'expires_in' => auth()->factory()->getTTL() * 60,
                ]
            ]);
        }
    }

    public function check()
    {
        try {
            if (! $token = JWTAuth::parseToken()) {
                return response(['authenticated' => false]);
            }    
            //JWTAuth::parseToken()->authenticate();
        } catch (JWTException $e) {
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException){
                return response(['authenticated' => false]);
            }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException){
                return response(['authenticated' => false]);
            } else if ( $e instanceof \Tymon\JWTAuth\Exceptions\JWTException) {
                return response(['authenticated' => false]);
            }else{
                return response(['authenticated' => false]);
            }
        }
        return response(['authenticated' => true]);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()    
    {   
        try {          
            if (! $token = JWTAuth::parseToken()) {
                return response(['authenticated' => false]);
            }
            else{
                return response(['authenticated' => true, 'username' => auth()->user()->name]);
            }
            
        }
        catch (JWTException $e) {
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException){
                return response(['authenticated' => false]);
            }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException){
                return response(['authenticated' => false, 'msg' => 'session expired']);
            } else if ( $e instanceof \Tymon\JWTAuth\Exceptions\JWTException) {
                return response(['authenticated' => false]);
            }else{
                return response(['authenticated' => false]);
            }
        }

        return response(['authenticate' => false]);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'token' => $token,
            'expires_in' => auth()->factory()->getTTL() * 60,
        ]);
    }
}
