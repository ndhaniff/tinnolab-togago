<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Helper\Yippi;
use App\User;
use JWTAuth;

class YippiController extends Controller
{   
    /**
     * Yippi Weblogin
     *
     * @param Request $request
     * @param Yippi $yippi
     * @return Response JSON
     */
    public function weblogin(Request $request,Yippi $yippi)
    {
        $response = $yippi->webLogin([
            'username' => $request->username,
            'password' => $request->password
            ]);

        if(array_key_exists('msg',$response)){
            return $response;
        }
        //check if exist
        $user = DB::table('users as u')
        ->join('yippi_users as yu', 'yu.user_id', '=', 'u.id')
        ->where('yu.uid', $response['uid'])
        ->orWhere('u.email', $response['email'] );
        
        if(!$user->exists())
        {   
            $user = $this->createNewUser([
                "uid" => $response['uid'],
                "email" => $response['email'],
                "username" => $response['username'],
                "phone" => $response['username'],
                "access_token" => $response['access_token'],
            ]);
        }
        else{
            $user = User::find($user->first()->user_id);
        }

        //login with User instance
        return $this->loginWithInstance($user);
       
    }

    /**
     * Login with User Instance
     *
     * @param [type] $user
     * @return Response JSON
     */
    public function loginWithInstance($user) {
        if(JWTAuth::fromUser($user)) {
            $token = JWTAuth::fromUser($user);
            $url = url()->current();
            $role = DB::table('roles')->where('id', $user->role['role_id'])->first();

            return response()->json([
                'success'=> true,
                'auth' => [
                    'access_token' => $token,
                    'name' => $user->name,
                    'email' => $user->email,
                    'role' => $role->name,
                    'bind_status' => $user->bind_status,
                    'expires_in' => auth()->factory()->getTTL() * 60,
                ]
            ]);
        }
        return response()->json([$user]);
    }

    /**
     * Create New User
     *
     * @param Array $args
     * @return User
     */
    public function createNewUser(Array $args)
    {
        //create user
        $user = new User();
        $user->email = $args['email'];
        $user->name = $args['username'];
        $user->password = bcrypt($args['username']);
        $user->bind_status = 2; //yippi only
        $user->save();

        //set role for user as customer
        $role = new \App\Role([
            "user_id" => $user->id,
            "role_id" => 2,
        ]);

        //create profile detail
        $profile = new \App\Model\Profile([
            "user_id" => $user->id,
        ]);
        
        //create yippi detail belongs to user
        $yippi = new \App\Model\YippiUser([
            "uid" => $args['uid'],
            "username" => $args['username'],
            "email" => $args['email'],
            "access_token" => $args['access_token'],
            "phone" => $args['phone']
        ]);

        $user->role()->save($role);
        $user->profile()->save($profile);
        $user->yippi()->save($yippi);

        return $user;
    }
}
