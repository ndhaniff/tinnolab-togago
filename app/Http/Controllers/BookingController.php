<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Model\Flight;
use App\Model\Payment;

class BookingController extends Controller
{   
    /**
     * Get Flight Booking List 📋
     *
     * @return Response JSON
     */ 
    public function flight ()
    {
       $user = Auth()->user();
       $flights = $user->flights()->get();
       $bookings = [];

       foreach($flights as $flight) {
           $data = (array) DB::table('bookings as b')
           ->select('b.booking_id', 'b.currency', 'b.amount', 'b.created_at as booked_date', 's.name as status')
           ->join('payments as p', 'p.booking_id' ,'=','b.booking_id')
           ->join('bookings_status as bs', 'bs.booking_id' ,'=','b.booking_id')
           ->join('status as s', 's.id' ,'=','bs.status_id')
           ->where('b.booking_id', $flight->booking_id)->first();

           //capitalize status
          if($data) {
            $data['status'] = ucwords(strtolower($data['status']));
          }

           //push to bookings
           array_push($bookings, array_merge($data, $flight->book_data));
       }
       
       if(count($flights) > 0) {
           return response()->json(["success" => true, "data" => ["booking" => $bookings]]);
       }
       else {
        return response()->json(["success" => false, "msg" => "record not found"]);
       }
    }
    
    /**
     * Check payment status for routing
     *
     * @param Request $request
     * @return Response JSON
     */
    public function checkPaymentStatus(Request $request)
    {
        $payment = Payment::where('ref_id', $request->ref_id)->first();
        if($payment) {
            return response()->json(['success' => true, 'status' => $payment->status_code]);
        }
        else {
            return response()->json(['success' => false, 'msg' => 'no record found']);
        }
    }

    //Authorize Only Request 👨‍✈️
    /**
     * Get User Booking Info
     *
     * @return void
     */
    public function getUserBookingInfo() {
        $user = auth()->user();
        $profile = ['email' => $user->email];

        if($user->profile['contact_info']) {
            $profile = array_merge($profile, unserialize($user->profile['contact_info']));
            return response()->json(['success' => true, "profile" => $profile]);
        }
        return response()->json(['success' => false]);
    }

}
