<?php

namespace App\Http\Controllers;

use App\Helper\Flight;
use App\Helper\Payments\Upay;
use App\User;
use Auth;
use App\Mail\sendCustomerTempPassword;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Mail;

class FlightController extends Controller
{
    protected $flightData = [];
    protected $token;

    /**
     * Flight Search
     *
     * @param Request $request
     * @param Flight $flight
     * @return Response JSON
     */
    public function search(Request $request, Flight $flight)
    {
        $children = [];
        $this->flightData = [];
        $this->filterData = [];
        
        if($request->has('token')) {
            $this->token = $request->token;
        }
        else{
            $this->token = $flight->getAccessToken();
        }
        
        $isRoundtrip = json_decode($request->isRoundtrip, true);

        if ($request->children) {
            $children = [];

            foreach ($request->children as $child) {
                if ("< 2" == $child) {
                    $child = 1;
                }
                array_push($children, $child);
            };
        }

        $params = [
            'token' => $this->token,
            'origin' => $request->origin,
            'destination' => $request->destination,
            'departureDate' => $request->departureDate,
            'departureDateReturn' => $request->departureDateReturn,
            'adults' => $request->adults,
            'children' => $children,
            'class' => $request->class,
            'isRoundtrip' => $isRoundtrip,
        ];

        /* CACHING SEARCH FOR 5 MINUTES */
        $key = http_build_query([
            'origin' => $request->origin,
            'destination' => $request->destination,
            'departureDate' => $request->departureDate,
            'departureDateReturn' => $request->departureDateReturn,
            'adults' => $request->adults,
            'children' => $children,
            'class' => $request->class,
            'isRoundtrip' => $isRoundtrip,
        ], 'flags_');

        if (!Cache::has($key)) {
            $searchData = $flight->search($params);
            Cache::put($key, $searchData, 5);
            $searchData = json_decode($searchData, true);
        } 
        else if(!is_array(Cache::get($key))) {
            $searchData = $flight->search($params);
            Cache::put($key, $searchData, 5);
            $searchData = json_decode($searchData, true);
        }
        else{
            $searchData = json_decode(Cache::get($key), true);
        }
        if (array_key_exists('error', $searchData)) {
            return $searchData["error"]["message"];
        }

        $entities = array_get($searchData, 'data.entities');
        $totalPage = array_get($searchData, 'data.properties.pages');
        $currentPage = array_get($searchData, 'data.properties.page');

        $this->filterData = array_get($searchData, 'data.filters');

        $this->iterateSegmentData($entities);

        
        return [
            'success' => true,
            'flightdata' => $this->flightData ,
            'filterdata' => $this->filterData ,
            'totalpages' => $totalPage,
            'currentpage' => $currentPage,
            "token" => $this->token
        ];
    }

    /**
     * Filter Results
     *
     * @param Request $request
     * @param Flight $flight
     * @return Response JSON
     */
    public function filter(Request $request, Flight $flight) 
    {   
        $filterData = $flight->filterSearch([
            'token' => $request->token,
            'filter' => $request->queryString,
        ]);
        
        $filterData = json_decode($filterData, true);

        if (array_key_exists('error', $filterData)) {
            return $filterData["error"]["message"];
        }

        $entities = array_get($filterData, 'data.entities');
        $currentPage = array_get($filterData, 'data.properties.page');

        $this->filterData = array_get($filterData, 'data.filters');

        $this->iterateSegmentData($entities);

        return [
            'success' => true,
            'flightdata' => $this->flightData ,
            'filterdata' => $this->filterData ,
            'currentpage' => $currentPage,
            "token" => $request->token
        ];

    }

    /**
     * Iterate and set Response Object
     *
     * @param [array] $datas
     * @return void
     */
    public function iterateSegmentData($datas)
    {   
        foreach ($datas as $data) {
            $args = [
                "itemId" => $data["itemId"],
                "price" => $data["price"],
                "links" => $data['links'],
            ];

            if (is_array($data["segments"])) {
                $index = 1;
                foreach ($data["segments"] as $key => $value) {
                    $args['segments'][] = $data["segments"][$key];
                }
            }
            array_push($this->flightData, $args);
        }
    }

    /**
     * Get Selected Flight Base On Search
     *
     * @param Request $request
     * @param Flight $flight
     * @return Response JSON
     */
    public function getSelected(Request $request, Flight $flight)
    {
        $args = [
            'token' => $request->token,
            'itemId' => $request->itemId,
        ];

        $response = json_decode($flight->select($args),true);
        
        if(!isset($response['error'])){
          return response()->json(['success' => true, 'data' => $response]);
        }
        return response()->json(['success' => false, 'error' => $response['error']['message']]);
    }

    /**
     * Save Booking Data
     *
     * @param Request $request
     * @param Flight $flight
     * @return Response JSON
     */
    public function saveBookingInfo(Request $request, Flight $flight)
    {
        //set passenger
        $passengers = $this->setPassenger([
            'adult' => $request->adult,
            'child' => $request->child,
            'infant' => $request->infant,
        ]);

        //set user info
        $contact = $request->contact;
        $profile_info = [
            'first_name' => $contact['firstName'],
            'last_name' => $contact['lastName'],
            'title' => $contact['title'],
            'mobile_prefix' => $contact['phoneprefix'],
            'mobile_phone' => $contact['phonenumber'],
            'email' => $contact['email'],
            'city' => $contact['city'],
            'zip' => $contact['zip'],
            'address' => $contact['zip'],
            'address' => $contact['zip'],
            'country_code' => $contact['country'],
        ];
        $password = str_random(8);
            
        //already logged in
        if(Auth::check()) {
            $user = Auth::user();
            $user->profile()->update([
                'contact_info' => serialize($profile_info)
            ]);
        }
        else {
            //check user exist
            $user = User::whereEmail($contact['email']);

            if(!$user->exists()) {
                //create user with temp pass if not exist
                $user = new User();
                $user->email = $contact['email'];
                $user->name = $contact['firstName'];
                $user->password = bcrypt($password);
                $user->bind_status = 1; //email only
                $user->save();
                
                //save contact info
                $profile = new \App\Model\Profile(array_merge($profile_info, ['contact_info' => serialize($profile_info)]));
                $user->profile()->save($profile);

                //set role
                $role = new \App\Role([
                    "user_id" => $user->id,
                    "role_id" => 2,
                ]);
                $user->role()->save($role);
            }
            else {
                $user = User::find($user->first()->id);
                $user->profile()->update([
                    'contact_info' => serialize($profile_info)
                ]);
                $user = $user->first();
            }
        } 

        //Book to multireisen
        $response = $flight->saveBookingInfo([
            'token' => $request->token,
            'itemId' => $request->itemId,
            'passengers' => $passengers,
            'contact' => $request->contact,
        ]);

        if(isset(json_decode($response,true)['error'])){
            return response()->json(['success' => false, 'error' => json_decode($response,true)['error']['message'] ]);
        }
        else {   

            //save flight
            $bookingId = json_decode($response, true)['data']['bookingId'];
            $currency = json_decode($response, true)['data']['price']['currency'];
            $total = json_decode($response, true)['data']['price']['total'];

            $flights = DB::table('flights')->where('booking_id', $bookingId);
            if(!$flights->exists()) {
                $flights = DB::table('flights')->insert([
                    'details' =>  json_encode($request->flight),
                    'passengers' => json_encode($passengers),
                    'booking_id' =>  $bookingId,
                    'user_id' => $user->id,
                    'origin' => $request->origin,
                    'destination' => $request->destination,
                    'departure' => $request->departure,
                    'departureDateReturn' => $request->departureDateReturn,
                    'isRoundTrip' => $request->isRoundTrip == "true" ? true : false,
                    'updated_at' => \Carbon\Carbon::now(),
                    'created_at' => \Carbon\Carbon::now()
                ]);
            }
            else {
                return response()->json(['success' => false, 'error' => 'Booking record already exist!' ]);
            }

            //save booking data
            $bookings =  DB::table('bookings')->where('booking_id', $bookingId);
            if(!$bookings->exists()) {
                $bookings = DB::table('bookings')->insert([
                    'booking_id' =>  $bookingId,
                    'has_booked' => true,
                    'currency' => $currency,
                    'booking_data' => $response,
                    'amount' => $total,
                    'updated_at' => \Carbon\Carbon::now(),
                    'created_at' => \Carbon\Carbon::now()
                ]);

                //update booking status
                $booking_status = DB::table('bookings_status')->insert([
                    'booking_id' => $bookingId,
                    'status_id' => 2, // set later
                    'updated_at' => \Carbon\Carbon::now(),
                    'created_at' => \Carbon\Carbon::now()
                ]);
            }
            else {
                return response()->json(['success' => false, 'error' => 'Booking record already exist!' ]);
            }


            $response = [
                'success' => true, 
                'message' => 'booking succeeded!'
            ];


            return response()->json($response);
        }
    }

    public function setPassenger(Array $data) {
        $passengers = [];
        if(count($data['adult']) != 0){
            $adult = $data['adult'];

            foreach($adult as $key => $value) {
              $adult[$key] = array_merge($adult[$key], [

                "options" => [
                    "1" => [
                        "id" => "OutwardLuggageOptions",
                        "value" => "1"
                    ],
                    "2" => [
                        "id" => "ReturnLuggageOptions",
                        "value" => "1"
                    ]
                  ]
              ]);
            }
            $passengers = array_merge($passengers, $adult);
          }
          if(count($data['child']) != 0){
            $passengers = array_merge($passengers, $data['child']);
          }
          if(count($data['infant']) != 0){
            $passengers = array_merge($passengers, $data['infant']);
          }

          return $passengers;
    }

    /**
     * Finalize Booking
     *
     * @param Request $request
     * @param Flight $flight
     * @return Response JSON
     */
    public function finalizeBooking(Request $request, Upay $upay)
    {   
        $user = User::where('email', $request->customer_email)->first();
        $flight = $user->flights()->latest()->first();
        $booking = $flight->booking()->latest()->first();
        $payment = $booking->payment()->latest()->first();

        //gateway
        if($request->gateway == "upay") {
            (int)$currency_rate = DB::table("currency_rate")->where("key", "=", "USD_MYR")->first()->rate;

            $bookingPrice = $request->final_price * $currency_rate;
            $refId = str_pad($booking->booking_id, 8, rand(1, 999999), STR_PAD_RIGHT);
            
            //create upay form data
            $upayForm = $upay->makePayment(array(
                "booking_id" => $refId,
                "user_id" => $user->id,
                "currency" => 'USD',
                "name" => $user->name,
                "email" => $user->email,
                "mobile_number" => $user->mobile_number,
                "customer_ip" => $request->ip(),
                "final_price" => $bookingPrice,
            ));

            //create payment record
            $payment = \App\Model\Payment::updateOrCreate([
                'type' => $request->gateway,
                'booking_id' => $booking->booking_id,
                'ref_id' => $refId,
                'details' => null,
                'status' => 'NOTPAID',
            ]);

        }

        return response()->json(["success" => true, "data" => $upayForm]);

    }

    public function processPayment(Request $request)
    {
        $payment = \App\Model\Payment::where('ref_id',$request->ORDER_ID)->first();
        $payment->update([
            'details' => serialize($request->all()),
            'status' => $request->STATUS_DESC
        ]);
        
        $locale = app()->getLocale();
        
        if($request->STATUS_CODE == '00') {
            //finalize to multireisen
            //deduct yippi or coupon
            //change booking status
            $booking = \App\Model\Booking::where('booking_id', $payment->booking_id)->first();
            $booking->update([
                "has_paid" => 1
            ]);
            $bookingStatus = DB::table('bookings_status')
            ->where('booking_id', $booking->booking_id)
            ->update([
                "status_id" => 1
            ]);

            //email user the voucher
            return redirect(url('/').'/'.$locale.'/flight/success/'.$request->ORDER_ID);
        }
        else {
            return redirect(url('/').'/'.$locale.'/flight/unsuccessfull'.$request->ORDER_ID);
        }
    }

    /**
     * Send Email
     *
     * @param array $args
     * @return void
     */
    public function sendMail(array $args)
    {
        Mail::to($args['email'])->send(new sendCustomerTempPassword($args));
    }

}
