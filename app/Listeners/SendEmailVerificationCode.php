<?php

namespace App\Listeners;

use App\Events\EmailVerification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Mail\SendEmailVerificationCode as SendVerifyCode;
use Illuminate\Support\Facades\Mail;

class SendEmailVerificationCode
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EmailVerification  $event
     * @return void
     */
    public function handle(EmailVerification $event)
    {
        $user = $event->user;
        $email = $event->email;

        $verification_code = strtoupper(str_random(5));
        $user->update(['verification_code' => $verification_code]);

        Mail::to($email)->send(new SendVerifyCode([
            'name' => $user->name,
            'email' => $email,
            'verification_code' => $user->verification_code
        ]));
        // try {
        //     Mail::to($email)->send(new SendVerifyCode([
        //         'name' => $user->name,
        //         'email' => $email,
        //         'verification_code' => $user->verification_code
        //     ]));
        // }
        // catch(\Exception $e) {
        //     return false;
        // }
    }
}
