<?php
/**
 * Flight API Helper
 * @author ndhaniff
 */
namespace App\Helper;

use GuzzleHttp\Client;

class Yippi 
{
  protected $http;
  protected $app_id;
  protected $app_secret;
  protected $api_url;

  public function __construct(Client $http)
  {
    $this->http = $http;
    $this->app_id = "5b6bf04f19aa3cd91562acc8d1a7d725eff85463";
    $this->app_secret = "72d322ac0ebea26f814613830ba9bc7c36b4ab895ab392737451aeb5467bd3db";
    $this->api_url = "http://chat2.pentajeu.com/yippi-oauth";
  }

  /**
   * Yippi Web login
   *
   * @param [array] $credentials
   * @return array
   */
  public function webLogin($credentials) 
  {
    $requestData = [
      "headers" => [
          "Content-Type" => "application/x-www-form-urlencoded",
      ],
      "form_params" => [
        "client_app_id" => $this->app_id,
        "client_app_secret" => $this->app_secret,
        "scope" => "user",
        "username" => $credentials['username'],
        "password" => $credentials['password'],
      ]
    ];

    $response = $this->execute("post", "/api/user/", $requestData);

    if($response['state']['code'] != 200) {
      return ["msg" => $response['state']['msg']];
    }
    return [
        "uid" => $response["data"]["uid"],
        "access_token" => $response["data"]["access_token"],
        "phone" => $response["data"]["phone"],
        "email" => $response["data"]["email"],
        "username" => $response["data"]["username"],
      ];
  }

  /**
   * Fetch the api
   *
   * @param [string] $type
   * @param [string] $endpoint
   * @param [array] $data
   * @return $response
   */
  public function execute($type, $endpoint, $data)
  {
    try {
      $response = $this->http->request($type, $this->api_url . $endpoint, $data);

      if($response->getStatusCode() == 200) {
        return json_decode($response->getBody()->getContents(), true);
      }
    }
    catch(\GuzzleHttp\Exception\ClientException $e){
      $response = $e->getResponse();
      return $responseBodyAsString = $response->getBody()->getContents();
    }
  }
}