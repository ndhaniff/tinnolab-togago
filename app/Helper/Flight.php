<?php
/**
 * Flight API Helper
 * @author ndhaniff
 * @api Multireisen 
 */
namespace App\Helper;

use GuzzleHttp\Client;

class Flight
{
    protected $http;
    protected $loginName;
    protected $password;
    protected $apiUrl = 'http://api2.multireisen.com';
    //protected $apiUrl = 'http://api2-test.multireisen.com';

    public function __construct(Client $http)
    {
        $this->http = $http;
        $this->loginName = env('MULTIREISEN_LOGIN');
        $this->password = env('MULTIREISEN_PASS');
        //$this->loginName = "keatwei@weavary.com";
        //$this->password = '123';
    }

    /**
     * Login to api
     * Using auth
     *
     * @return Json Response
     */
    public function login()
    {

        $requestData = [
            "headers" => [
                "Accept" => "application/json",
                "Content-Type" => "application/json",
            ],
            'auth' => [$this->loginName, $this->password],
        ];

        return $this->execute('GET', '/gettoken', $requestData);
    }

    /**
     * Get access token
     * from login response
     *
     * @return AccessToken
     */
    public function getAccessToken()
    {
        return json_decode($this->login(), true)['accessToken'];
    }

    /**
     * Search for flight using
     * basic auth and token
     *
     * @param [array] $data
     * @return Json Response
     */
    public function search(array $data)
    {

        $segments = [
            [
                "originCode" => $data['origin'],
                "destinationCode" => $data['destination'],
                "departureDate" => $data['departureDate'],
                "departureTime" => "0",
            ],
        ];

        if (true == $data['isRoundtrip']) {
            $segments[] = [
                "originCode" => $data['destination'],
                "destinationCode" => $data['origin'],
                "departureDate" => $data['departureDateReturn'],
                "departureTime" => "0",
            ];
        }

        $requestData = [
            "headers" => [
                "AccessToken" => $data['token'],
                "Accept" => "application/json",
                "Content-Type" => "application/json",
                'Authorization' => 'Basic ' . base64_encode($this->loginName . ':' . $this->password),
            ],
            "json" => [
                "adults" => $data['adults'],
                "children" => $data['children'],
                "segments" => $segments,
                "flexSearch" => "0",
                "stops" => "-1",
                "class" => $data['class'],
            ],
        ];

        return $this->execute('POST', '/flights/search', $requestData);
    }

    /**
     * Filter Search
     *
     * @param array $args
     * @return array
     */
    public function filterSearch(array $args)
    {
        $requestData = [
            "headers" => [
                "AccessToken" => $args['token'],
                "Accept" => "application/json",
                "Content-Type" => "application/json",
                "Version" => "v1",
                'Authorization' => 'Basic ' . base64_encode($this->loginName . ':' . $this->password),
            ]];

        $endpoint = '/flights/items/?' . $args['filter'];

        return $this->execute('GET', $endpoint, $requestData);
    }

    /**
     * Select Flight Base on Search
     *
     * @param array $data
     * @return Response Json
     */
    public function select(array $data)
    {
        $requestData = [
            "headers" => [
                "AccessToken" => $data['token'],
                "Accept" => "application/json",
                "Content-Type" => "application/json",
                "Version" => "v1",
                'Authorization' => 'Basic ' . base64_encode($this->loginName . ':' . $this->password),
            ]];

        $endpoint = '/flights/items/' . $data['itemId'] . '/select';

        return $this->execute('GET', $endpoint, $requestData);
    }

    /**
     * Save Booking Data
     *
     * @param array $data
     * @return array
     */
    public function saveBookingInfo(array $data)
    {
        $requestData = [
            "headers" => [
                "AccessToken" => $data['token'],
                "Accept" => "application/json",
                "Content-Type" => "application/json",
                "Version" => "v1",
                'Authorization' => 'Basic ' . base64_encode($this->loginName . ':' . $this->password),
            ],
            "json" => [
                "passengers" => $data['passengers'],
                "contact" => $data['contact'],
                "invoice" => [
                    "name" => "Multiresisen Gmbh.",
                    "country" => "DE",
                    "zip" => "34663",
                    "city" => "Berlin",
                    "address" => "Some street 1.",
                ],
                "invoice" => [
                  "name" => "Multiresisen Gmbh.",
                  "country" => "DE",
                  "zip" => "34663",
                  "city" => "Berlin",
                  "address" => "Some street 1."
                ],
                "options" => [
                    [
                        "id" => "SpeedyBoarding",
                        "value" => "1"
                    ]
                ],
                "paymentId" => "9"
            ],
        ];

        $endpoint = '/flights/items/' . $data['itemId'] . '/passengers';

        return $this->execute('POST', $endpoint, $requestData);
    }

    /**
     * Finalize Booking
     *
     * @param array $data
     * @return array
     */
    public function finalize(array $data)
    {

        $requestData = [
            "headers" => [
                "AccessToken" => $data['token'],
                "Accept" => "application/json",
                "Content-Type" => "application/json",
                "Version" => "v1",
                'Authorization' => 'Basic ' . base64_encode($this->loginName . ':' . $this->password),
            ]];

        $endpoint = '/flights/items/' . $data['itemId'] . '/book/' . $data['bookingId'];

        return $this->execute('PUT', $endpoint, $requestData);
    }

    /**
     * Execute api endpoints
     *
     * @param [string] $type
     * @param [string] $endpoint
     * @param [array] $data
     * @return Response Json
     */
    public function execute($type, $endpoint, $data)
    {
        try {
            $response = $this->http->request($type, $this->apiUrl . $endpoint, $data);
            if ($response->getStatusCode() == 200) {
                return $response->getBody()->getContents();
            }
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $response = $e->getResponse();
            return $responseBodyAsString = $response->getBody()->getContents();
        }
    }
}
