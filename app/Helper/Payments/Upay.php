<?php
/**
 * Upay Gateway Payment Helper
 * @author ndhaniff
 * @api Upay 
 */
namespace App\Helper\Payments;

use GuzzleHttp\Client;
use GuzzleHttp\TransferStats;

class Upay
{ 
  protected $http;
  protected $upay_merchantID;
  protected $upay_verifykey;
  protected $is_test;

  public function __construct(Client $http)
  {
      $this->http = $http;
      //$this->loginName = env('MULTIREISEN_LOGIN');
      //$this->password = env('MULTIREISEN_PASS');
      $this->upay_merchantID = "TOGL_TECHNOLOGY"; //env('MERCHANT_ID');
      $this->upay_verifykey = "c1e69c7e"; //env('MERCHANT_PASS');
      $this->is_test = "Y"; //env("PAYMENT_MODE")
  }

  public function makePayment(array $args)
  { 
    //dd($this->upay_merchantID);
    $transaction_id = $args["booking_id"];
    $currency = $args['currency'];
    $amount = number_format((float)$args["final_price"], 2, '.', '');
    $trxdescs = 'Togago booking Order ID: '.$transaction_id.' , '.$currency .' ' .$amount;
    $customer_id = $args['user_id'];
    $customer_name = $args['name'];
    $customer_email = $args['email'];
    $customer_mobile = $args['mobile_number'];
    $customer_country = '';
    $customer_billing_address = '';
    $customer_ip = $args['customer_ip'];
    $callback_url = url('/').'/api/flight/process_payment';

    if ($this->is_test == 'N') {///LIVE URL
        $upay_url = 'https://www.upay2us.com/iServeGateway/transaction_window';
        ///txn signature////
        $signature = $this->upay_verifykey . $this->upay_merchantID . number_format((float)$amount, 2, '.', '') . $customer_id . $transaction_id;
        $signature = str_pad($signature, 40, '0', STR_PAD_LEFT);
        $signature = hash('SHA512', $signature);
    } else {///UAT URL
        $upay_url = 'https://www.upay2us.com/iServeGateway_UAT/transaction_window';
        ///txn signature////
        $signature = $this->upay_verifykey . $this->upay_merchantID . number_format((float)$amount, 2, '.', '') . $customer_id . $transaction_id;
        $signature = hash('SHA512', $signature);
        $signature = str_pad($signature, 128, '0', STR_PAD_LEFT);
    }

    return $form_data = array(
        'MERCHANT_IDENTIFIER' => $this->upay_merchantID,
        'AMOUNT' => $amount,
        'TXN_DESC' => $trxdescs , 
        'CUSTOMER_ID' => $customer_id,
        'ORDER_ID' => $transaction_id,
        'INSTALLMENT' => 0, //installment not ready yet
        'CUSTOMER_NAME' => $customer_name,
        'CUSTOMER_EMAIL' =>  $customer_email, 
        'CUSTOMER_COUNTRY' =>  '',
        'CUSTOMER_BILLING_ADDRESS' => '',
        'CUSTOMER_SHIPPING_COST' => '',
        'CUSTOMER_IP' => $customer_ip, //
        'CALLBACK_URL' => $callback_url,
        'TXN_SIGNATURE' => $signature,
        'IS_TEST' => $this->is_test,
        'SOURCE_FROM' => 'WEB',
        'UPAY_URL' => $upay_url,
    );

  }

}