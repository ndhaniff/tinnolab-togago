<?php
/**
 * Flight API Helper
 * @author ndhaniff
 */
namespace App\Helper;

use GuzzleHttp\Client;

class Hotel
{
    protected $http;
    protected $loginName;
    protected $password;
    protected $apiUrl;

    public function __construct(Client $http)
    {
        $this->http = $http;

        $this->apiUrl = env('MULTIREISEN_API');
        $this->loginName = env('MULTIREISEN_LOGIN');
        $this->password = env('MULTIREISEN_PASS');
    }

    /**
     * Login to api
     * Using auth
     *
     * @return Json Response
     */
    public function login()
    {
        $requestData = [
            "headers" => [
                "Accept" => "application/json",
                "Content-Type" => "application/json",
            ],
            'auth' => [$this->loginName, $this->password],
        ];

        return $this->execute('GET', '/gettoken', $requestData);
    }

    /**
     * Get access token
     * from login response
     *
     * @return AccessToken
     */
    public function getAccessToken()
    {
        return json_decode($this->login(), true)['accessToken'];
    }

    /**
     * Search for flight using
     * basic auth and token
     *
     * @param [array] $data
     * @return Json Response
     */
    public function search(array $data)
    {
        $requestData = [
            "headers" => [
                "AccessToken" => $data['token'],
                "Accept" => "application/json",
                "Content-Type" => "application/json",
                'Authorization' => 'Basic ' . base64_encode($this->loginName . ':' . $this->password),
            ],
            "json" => [
                "cityId" => $data['cityId'],
                "checkIn" => $data['checkIn'],
                "checkOut" => $data['checkOut'],
                "rooms" => $data['rooms'],
                "category" => $data['category'],
                "nationality" => $data['nationality'],
                "residence" => $data['residence'],
            ],
        ];

        return $this->execute('POST', '/hotels/search', $requestData);
    }

    public function result(array $args)
    {
        $requestData = [
            "headers" => [
                "AccessToken" => $args['token'],
                "Accept" => "application/json",
                "Content-Type" => "application/json",
                "Version" => "v1",
                'Authorization' => 'Basic ' . base64_encode($this->loginName . ':' . $this->password),
            ],
        ];
        $endpoint = '/hotels/items?page=' . $args['page'];

        return $this->execute('GET', $endpoint, $requestData);
    }

    /**
     * Select Flight Base on Search
     *
     * @param array $data
     * @return Response Json
     */
    public function select(array $data)
    {
        $requestData = [
            "headers" => [
                "AccessToken" => $data['token'],
                "Accept" => "application/json",
                "Content-Type" => "application/json",
                "Version" => "v1",
                'Authorization' => 'Basic ' . base64_encode($this->loginName . ':' . $this->password),
            ]];

        $endpoint = '/flights/items/' . $data['itemId'] . '/select';

        return $this->execute('GET', $endpoint, $requestData);
    }

    public function saveBookingInfo(array $data)
    {
        $requestData = [
            "headers" => [
                "AccessToken" => $data['token'],
                "Accept" => "application/json",
                "Content-Type" => "application/json",
                "Version" => "v1",
                'Authorization' => 'Basic ' . base64_encode($this->loginName . ':' . $this->password),
            ],
            "json" => [
                "passengers" => $data['passengers'],
                "contact" => $data['contact'],
                "invoice" => [
                    "name" => "Multiresisen Gmbh.",
                    "country" => "DE",
                    "zip" => "34663",
                    "city" => "Berlin",
                    "address" => "Some street 1.",
                ],
                "invoice" => [
                    "name" => "Multiresisen Gmbh.",
                    "country" => "DE",
                    "zip" => "34663",
                    "city" => "Berlin",
                    "address" => "Some street 1.",
                ],
                "options" => [
                    [
                        "id" => "SpeedyBoarding",
                        "value" => "1",
                    ],
                ],
                "paymentId" => "11",
            ],
        ];

        $endpoint = '/flights/items/' . $data['itemId'] . '/passengers';

        return $this->execute('POST', $endpoint, $requestData);
    }

    /**
     * Execute api endpoints
     *
     * @param [string] $type
     * @param [string] $endpoint
     * @param [array] $data
     * @return Response Json
     */
    public function execute($type, $endpoint, $data)
    {
        try {
            $response = $this->http->request($type, $this->apiUrl . $endpoint, $data);
            if ($response->getStatusCode() == 200) {
                return $response->getBody()->getContents();
            }
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $response = $e->getResponse();
            return $responseBodyAsString = $response->getBody()->getContents();
        }
    }
}
