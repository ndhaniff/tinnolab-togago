<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('booking_id')->unique();
            $table->double('booking_fee', 8, 2)->default(0);
            $table->boolean('has_paid')->default(false);
            $table->boolean('has_booked')->default(false);
            $table->string('currency');
            $table->longText('booking_data');
            $table->integer('amount');
            $table->integer('final_amount')->nullable();
            $table->unsignedInteger('payment_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
