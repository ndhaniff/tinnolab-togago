<?php

use Illuminate\Database\Seeder;

class CitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('cities')->insert([
            'label' => str_random(10),
            'label_zh' => str_random(10),
            'id' => str_random(6),
            'category' => str_random(10),
            'categoryid' => str_random(10),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);

        DB::table('cities')->insert([
            'label' => 'Penang',
            'label_zh' => '槟城',
            'id' => '100',
            'category' => 'Malaysia',
            'categoryid' => 'my',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
        DB::table('cities')->insert([
            'label' => 'Kuala Lumpur',
            'label_zh' => '吉隆坡',
            'id' => '101',
            'category' => 'Malaysia',
            'categoryid' => 'my',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
        DB::table('cities')->insert([
            'label' => 'Malacca',
            'label_zh' => '马六甲',
            'id' => '102',
            'category' => 'Malaysia',
            'categoryid' => 'my',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
    }
}
