<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            
            'name' => 'Tinnolab',
            'email' => 'admin@admin.com',
            'password' => bcrypt('secret'), 
            'verification_code' => str_random(30),
            'verified' => 1,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);

        DB::table('profiles')->insert([
            'user_id' => 1,
            'first_name' => 'John',
            'last_name' => 'Doee',
            'country_code' => 'MY',
            'zip' => '06000',
            'city' => 'Jitra',
            'mobile_prefix' => '+60',
            'mobile_number' => '114875126',
            'address' => 'Taman Melur',
            'contact_info' => 'a:11:{s:7:"user_id";i:1;s:5:"title";s:2:"MR";s:10:"first_name";s:4:"John";s:9:"last_name";s:4:"Doee";s:12:"country_code";s:2:"MY";s:3:"zip";s:5:"06000";s:4:"city";s:5:"Jitra";s:13:"mobile_prefix";s:3:"+60";s:13:"mobile_number";s:9:"114875126";s:7:"address";s:11:"Taman Melur";s:5:"email";s:15:"admin@admin.com";}',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
    }
}
