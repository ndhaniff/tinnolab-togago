const mix = require("laravel-mix");
const WebpackShellPlugin = require("webpack-shell-plugin");

/*
|--------------------------------------------------------------------------
| Mix Asset Management
|--------------------------------------------------------------------------
|
| Mix provides a clean, fluent API for defining some Webpack build steps
| for your Laravel application. By default, we are compiling the Sass
| file for the application as well as bundling up all the JS files.
|
 */

var plugin = "resources/assets/plugins/";

mix.js("resources/assets/js/app.js", "public/js/app.js")
    .combine(
        [
            plugin + "jquery/jquery.min.js",
            plugin + "popper/popper.min.js",
            plugin + "bootstrap/bootstrap.min.js",
            plugin + "moment/moment-with-locales.min.js",
            plugin + "toastr/toastr.min.js",
            plugin + "slimscroll/jquery.slimscroll.js",
            plugin + "waves/waves.js",
            plugin + "sticky-kit/sticky-kit.min.js",
            plugin + "datatables/datatables.min.js",
            "resources/assets/js/custom.js",
            "public/js/messages.js",
            "public/js/app.js"
        ],
        "public/js/bundle.min.js"
    )
    .webpackConfig({
        plugins: [
            new WebpackShellPlugin({
                onBuildStart: [
                    "php artisan lang:js public/js/messages.js --quiet"
                ],
                onBuildEnd: []
            })
        ],
        resolve: {
            alias: {
                "@": path.resolve("resources/assets/sass")
            }
        }
    })
    .sass("resources/assets/sass/app.scss", "public/css")
    .browserSync("localhost:8000")
    .options({
        uglify: {
            uglifyOptions: {
                output: {
                    max_line_len: false
                }
            }
        }
    });
